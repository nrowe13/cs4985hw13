﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Suppliers.aspx.cs" Inherits="Suppliers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chapter 13: Northwind</title>
    <link href="Main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        &nbsp;<img alt="Northwind Solutions" src="Images/Northwind.jpg" /><br />
    </header>
    <section>
        <form id="form1" runat="server">
            <label>Choose a country:&nbsp;</label>
            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" DataSourceID="sdsCountry" DataTextField="Country" DataValueField="Country"></asp:DropDownList>
            <asp:SqlDataSource ID="sdsCountry" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT DISTINCT [Country] FROM [tblSuppliers] ORDER BY [Country]"></asp:SqlDataSource>
            <br />
            <br />
            <asp:DataList ID="dlSuppliers" runat="server" DataSourceID="sdsSuppliers">
                <HeaderTemplate>
                    <table>
                        <tr>
                            <td class="col2">Company Name</td>
                            <td class="col2">Phone Number</td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td class="col2">
                                <asp:Label ID="lblCompanyName" runat="server"
                                    Text='<%# Eval("CompanyName") %>' />
                            </td>
                            <td class="col2">
                                <asp:Label ID="lblPhoneNumber" runat="server"
                                    Text='<%# Eval("Phone") %>' />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <AlternatingItemStyle BackColor="#CCCCCC" />
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            </asp:DataList>
            <asp:SqlDataSource ID="sdsSuppliers" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT CompanyName, Phone FROM tblSuppliers WHERE (Country = ?)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlCountry" Name="Country" PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>
            <br />
        </form>
    </section>
</body>
</html>
